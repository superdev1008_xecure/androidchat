# Status: Archived
This repository has been archived and is no longer maintained.

![status: inactive](https://img.shields.io/badge/status-inactive-red.svg)


## Firebase Chat for Android

A simple chat application that built with Firebase on Android.

![Screenshot](screenshot.png)

## Setup

Update [`MainActivity`](/app/src/main/java/com/firebase/androidchat/MainActivity.java) and replace
with a reference to your Firebase.

## What's here

This application's
[`FirebaseListAdapter`](/app/src/main/java/com/firebase/androidchat/FirebaseListAdapter.java)
demonstrates binding Firebase to an Android
Firebase keeps the list data up to date based on a mapping to a model class.

## More about Firebase on Android

You can do lots more with Firebase on Android. Check out our Android to learn more.
